package com.andr2i.pollutionmonitor.view;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.GradientDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.messaging.FirebaseMessaging;
import com.andr2i.pollutionmonitor.AqiViewModel;
import com.andr2i.pollutionmonitor.data.model.Data;
import com.andr2i.pollutionmonitor.data.model.Iaqi;
import com.andr2i.pollutionmonitor.data.model.Pollutant;
import com.andr2i.pollutionmonitor.R;
import com.andr2i.pollutionmonitor.data.api.RetrofitHelper;
import com.andr2i.pollutionmonitor.data.model.Status;
import com.andr2i.pollutionmonitor.view.adapters.PollutantsAdapter;
import com.andr2i.pollutionmonitor.data.gps.GPSUtils;


import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {


    //Views
    private TextView aqiTextView, temperatureTextView, locationTextView, pressureTextView, humidityTextView, windTextView;
    private RecyclerView pollutantsRecyclerView;
    private ImageView aqi_background;

    //Data
    private AqiViewModel aqiViewModel;
    private Data data = new Data();
    private PollutantsAdapter pollutantsAdapter;
    private List<Pollutant> pollutantsList = new ArrayList<>();


    //Location
    private FusedLocationProviderClient fusedLocationClient;
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private LocationManager locationManager;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private Location latestLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTheme(R.style.AppTheme_Dark);
        setContentView(R.layout.activity_main);
        init();
        aqiViewModel = ViewModelProviders.of(this).get(AqiViewModel.class);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.
                PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(20 * 1000);
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        latestLocation = location;
                        getAqiData(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));
                    }
                }
            }
        };
        checkGPSAndRequestLocation();
        try {
            FirebaseMessaging.getInstance().subscribeToTopic("weather")
                    .addOnCompleteListener(task -> Log.d("FCM", "Subscribed to \"weather\""));
        } catch (Exception e) {
            Log.e("FCM", "Unable to add FCM topic");
        }
    }

    private void checkGPSAndRequestLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                checkLocationPermission();
            } else {
                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    //Call for AQI data based on location is done in "locationCallback"
                    fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
                } else {
                    new GPSUtils(this).turnGPSOn();
                }
            }
        }
    }


    private void init() {
        aqiTextView = findViewById(R.id.aqi_text_view);
        temperatureTextView = findViewById(R.id.temperature_text_view);
        locationTextView = findViewById(R.id.location_text_view);
        pressureTextView = findViewById(R.id.pressure_text_view);
        humidityTextView = findViewById(R.id.humidity_text_view);
        windTextView = findViewById(R.id.wind_text_view);
        aqi_background = findViewById(R.id.aqi_background);
        setupRecyclerView();
    }


    private void setupRecyclerView() {
        pollutantsRecyclerView = findViewById(R.id.pollutants_recycler_view);
        pollutantsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        pollutantsRecyclerView.setHasFixedSize(true);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(pollutantsRecyclerView.getContext(),
                DividerItemDecoration.VERTICAL);
        pollutantsRecyclerView.addItemDecoration(dividerItemDecoration);
    }

    private void addPollutantsToList(Iaqi iaqi) {
        pollutantsList.clear();
        if (iaqi.getCo() != null)
            pollutantsList.add(new Pollutant("Carbon Monoxide - AQI", iaqi.getCo().getV()));
        if (iaqi.getNo2() != null)
            pollutantsList.add(new Pollutant("Nitrous Dioxide - AQI", iaqi.getNo2().getV()));
        if (iaqi.getO3() != null)
            pollutantsList.add(new Pollutant("Ozone - AQI", iaqi.getO3().getV()));
        if (iaqi.getPm2_5() != null)
            pollutantsList.add(new Pollutant("PM 2.5 - AQI", iaqi.getPm2_5().getV()));
        if (iaqi.getPm10() != null)
            pollutantsList.add(new Pollutant("PM 10 - AQI", iaqi.getPm10().getV()));
        if (iaqi.getSo2() != null)
            pollutantsList.add(new Pollutant("Sulfur Dioxide - AQI", iaqi.getSo2().getV()));
        pollutantsAdapter = new PollutantsAdapter(pollutantsList);
        pollutantsRecyclerView.setAdapter(pollutantsAdapter);
    }

    private void setAqiScaleGroup() {
        int aqi = data.getAqi();
        int color = 0;
        TextView aqiScaleText;

//        if (aqi >= 0 && aqi <= 50)
//            aqiScaleText = findViewById(R.id.scaleGood);
//        else if (aqi >= 51 && aqi <= 100)
//            aqiScaleText = findViewById(R.id.scaleModerate);
//        else if (aqi >= 101 && aqi <= 150)
//            aqiScaleText = findViewById(R.id.scaleUnhealthySensitive);
//        else if (aqi >= 151 && aqi <= 200)
//            aqiScaleText = findViewById(R.id.scaleUnhealthy);
//        else if (aqi >= 201 && aqi <= 300) aqiScaleText = findViewById(R.id.scaleVeryUnhealthy);
//        else if (aqi >= 301)
//            aqiScaleText = findViewById(R.id.scaleHazardous);
//        else
//            aqiScaleText = findViewById(R.id.scaleGood);


        switch ((int) aqi / 50) {
            case 0:
               // aqiScaleText = findViewById(R.id.scaleGood);
                color = getResources().getColor(R.color.scaleGood);
                break;
            case 1://(aqi >= 51 && aqi <= 100)
               // aqiScaleText = findViewById(R.id.scaleModerate);
                color = getResources().getColor(R.color.scaleModerate);
                break;
            case 2://(aqi >= 101 && aqi <= 150)
                color = color = getResources().getColor(R.color.scaleUnhealthySensitive);
               // aqiScaleText = findViewById(R.id.scaleUnhealthySensitive);
                break;
            case 3://(aqi >= 151 && aqi <= 200)
                color = getResources().getColor(R.color.scaleUnhealthy);
               // aqiScaleText = findViewById(R.id.scaleUnhealthy);
                break;
            case 4:case 5://(aqi >= 201 && aqi <= 300)
                color = getResources().getColor(R.color.scaleVeryUnhealthy);
               // aqiScaleText = findViewById(R.id.scaleVeryUnhealthy);
                break;
            default://(aqi >= 301)
                color = getResources().getColor(R.color.scaleHazardous);
               // aqiScaleText = findViewById(R.id.scaleHazardous);
                break;
        }
       GradientDrawable backgraund = (GradientDrawable) aqi_background.getDrawable();
       backgraund.setStroke(4,color);
           // d.setColorFilter((int)R.color.scaleHazardous, PorterDuff.Mode.DST);

      //  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      //      aqiScaleText.setForeground(getDrawable(R.drawable.selected_aqi_foreground));


        //}
    }

    private void showDialog(String s) {
        RetrofitHelper.getInstance().showProgressDialog(this, s);
    }

    private void dismissDialog() {
        RetrofitHelper.getInstance().dismissProgressDialog();
    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle(R.string.alert_title_location_access)
                        .setMessage(R.string.alert_content_location_access)
                        .setPositiveButton("Ok", (dialogInterface, i) -> {
                            //Prompt the user once explanation has been shown
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    MY_PERMISSIONS_REQUEST_LOCATION);
                        })
                        .setNegativeButton("Cancel", (dialogInterface, i) -> {
                            getAqiData();
                        })
                        .create()
                        .show();
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        //Request location updates:
                        checkGPSAndRequestLocation();
                    }

                } else {
                    getAqiData();
                }
            }
        }
    }

    private void getAqiData(String latitude, String longitude) {
        String geo = "geo:" + latitude + ";" + longitude;
        aqiViewModel.getStatus().observe(MainActivity.this, status -> {
            if (status != null) {
                if (status == Status.FETCHING) {
                    showDialog("Getting data from nearest station...");
                } else dismissDialog();
            }
        });
        aqiViewModel.getGPSApiResponse(geo).observe(MainActivity.this, apiResponse -> {
            if (apiResponse != null) {
                Log.d("api", String.valueOf(apiResponse));
                data = apiResponse.getData();
                aqiTextView.setText(String.valueOf(data.getAqi()));


                setAqiScaleGroup();
                Iaqi iaqi = data.getIaqi();
                if (iaqi.getTemperature() != null)
                    temperatureTextView.setText(getString(R.string.temperature_unit_celsius, data.getIaqi().getTemperature().getV()));
                if (iaqi.getPressure() != null)
                    pressureTextView.setText(getString(R.string.pressure_unit, iaqi.getPressure().getV()));
                if (iaqi.getHumidity() != null)
                    humidityTextView.setText(getString(R.string.humidity_unit, iaqi.getHumidity().getV()));
                if (iaqi.getWind() != null)
                    windTextView.setText(getString(R.string.wind_unit, iaqi.getWind().getV()));
                locationTextView.setText(data.getCity().getName());
                // setupAttributions(data);
                addPollutantsToList(data.getIaqi());
                pollutantsAdapter.notifyDataSetChanged();
            }
        });
    }

    private void getAqiData() {
        aqiViewModel.getStatus().observe(MainActivity.this, status -> {
            if (status != null) {
                if (status == Status.FETCHING) {
                    showDialog("Getting data based on network...");
                } else dismissDialog();
            }
        });
        aqiViewModel.getApiResponse().observe(MainActivity.this, apiResponse -> {
            if (apiResponse != null) {
                Log.d("api", String.valueOf(apiResponse));
                data = apiResponse.getData();
                aqiTextView.setText(String.valueOf(data.getAqi()));


                setAqiScaleGroup();
                Iaqi iaqi = data.getIaqi();
                if (iaqi.getTemperature() != null)
                    temperatureTextView.setText(getString(R.string.temperature_unit_celsius, data.getIaqi().getTemperature().getV()));
                if (iaqi.getPressure() != null)
                    pressureTextView.setText(getString(R.string.pressure_unit, iaqi.getPressure().getV()));
                if (iaqi.getHumidity() != null)
                    humidityTextView.setText(getString(R.string.humidity_unit, iaqi.getHumidity().getV()));
                if (iaqi.getWind() != null)
                    windTextView.setText(getString(R.string.wind_unit, iaqi.getWind().getV()));
                locationTextView.setText(data.getCity().getName());
                //  setupAttributions(data);
                addPollutantsToList(data.getIaqi());
                pollutantsAdapter.notifyDataSetChanged();
            }
        });
    }


}
