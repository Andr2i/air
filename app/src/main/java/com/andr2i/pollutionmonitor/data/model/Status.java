package com.andr2i.pollutionmonitor.data.model;

public enum Status {
    FETCHING,
    DONE,
    ERROR
}
